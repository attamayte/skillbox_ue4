// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "learn_ue4GameMode.generated.h"

UCLASS(minimalapi)
class Alearn_ue4GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	Alearn_ue4GameMode();
};



