// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "learn_ue4GameMode.h"
#include "learn_ue4Character.h"
#include "UObject/ConstructorHelpers.h"

Alearn_ue4GameMode::Alearn_ue4GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
